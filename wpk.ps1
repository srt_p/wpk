# Preferences
# use TLS 1.2
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
# don't display powershell process bars
$ProgressPreference = "SilentlyContinue"

$wpk_help = @"
wpk - windows package management

Installing and removing packages:
  add       install (or upgrade) packages including their dependencies
  del       uninstall packages

Querying information about packages:
  info      give detailed information about packages
  list      list packages by pattern and other criteria
"@
$wpk_help_globalargs = @"

global options:
  -h, --help         show generic help or applet specific help
  -q, --quiet        print less information
  -v, --verbose      print more information
"@
$wpk_add_help = @"
wpk - windows package management

usage: apk add [options] PACKAGE ...

Description:
  install (or upgrade) PACKAGEs including their dependencies

add options:
  -u, --upgrade       prefer to upgrade packages
"@
$wpk_script_directory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$wpk_script = $MyInvocation.MyCommand.Definition
$wpk_args = $args

Write-Host "wpk_script_dir: $($wpk_script_directory)"

class wpk_package {
    [string[]] dependencies() {return @()}

    [string[]] inner_dependencies() {
        # inner dependencies are installed into the same folder as the program,
        # e.g. if program needs some extra library files
        return @()
    }

    [string] download_url() {
        # return the url to the installer file
        return ""
    }

    [string[][]] shims() {
        # return the files relative to the installation directory (path -> index 0) that should
        # be added to PATH, called with arguments (index 1) and named in shim-folder (like index 1)
        return @()
    }

    [string] installertype() {return ""}

    [string] fileext() {
        switch ($this.installertype()) {
            "innosetup" {return "exe"}
            "zip" {return "zip"}
            "nsis" {return "exe"}
            "msi" {return "msi"}
            "7z" {return "7z"}
        }
        return ""
    }

    [string] description() {
        return ""
    }

    [string[][]] checksums() {
        # return the checksum algorithm (index 0) and the checksum (index 1)
        return @()
    }

    [string[]] winver() {
        # return supported windows versions
        return @("win32","win64")
    }

    install($sFilename, $sInstallDir) {
        switch ($this.installertype()) {
            "innosetup" {
                Start-Process -Wait -FilePath "$($sFilename)" -ArgumentList `
                    @("/SP-","/VERYSILENT","/NORESTART","/NOICONS","/SUPPRESSMSGBOXES","/DIR=`"$($sInstallDir)`"")
                break
            }
            "zip" {
                Expand-Archive "$($sFilename)" -DestinationPath "$($sInstallDir)"
                break
            }
            "nsis" {
                Start-Process -Wait -FilePath "$($sFilename)" -ArgumentList `
                    @("/S","/D=$($sInstallDir)")
                break
            }
            "msi" {
                Start-Process -Wait -FilePath "msiexec.exe" -ArgumentList `
                    @("/i","$($sFilename)","INSTALLDIR=`"$($sInstallDir)`"","/qn")
                break
            }
            "7z" {
                if ((Get-Command "7z" -ErrorAction SilentlyContinue) -eq $null) {
                    Write-Host "ERROR: '7z' is not an executable. Install e.g. with 'wpk add 7zip'."
                    exit 1
                }
                Start-Process -NoNewWindow -Wait -FilePath "cmd" -ArgumentList `
                    @("/c","7z","x","-aoa","-o`"$sInstallDir`"","$sFilename")
                break
            }
        }
    }

    pre_install() {}
    post_install() {}
}

class wpk {
    static $oAdminProcess = $null
    static $oAdminPipeIn = $null
    static $oAdminPipeInReader = $null
    static $oAdminPipeOut = $null
    static $oAdminPipeOutWriter = $null

    static help($dGlobalArgs) {
        Write-Host ${global:wpk_help}
        if ($dGlobalArgs["verbose"] -gt 0) {
            [wpk]::help_globalargs()
        }
    }

    static help_globalargs() {
        Write-Host ${global:wpk_help_globalargs}
    }

    static exec($aArgs) {
        $dGlobalArgs = @{
            quiet = 0
            verbose = 0
            help = 0
            force_refresh = 0
        }
        $aRestArgs = @()
        $sOperation = ""
        for ($i = 0; $i -lt $aArgs.length; $i++) {
            switch -regex ($aArgs[$i].ToLower()) {
                "^(-q|--quiet)$" {$dGlobalArgs["quiet"]++; break}
                "^(-v|--verbose)$" {$dGlobalArgs["verbose"]++; break}
                "^(-h|--help)$" {$dGlobalArgs["help"]++; break}
                "^--force-refresh$" {$dGlobalArgs["force_refresh"]++; break}
                default {
                    # if the argument is not starting with a dash it should be the operation,
                    # unless operation is already set
                    if ($sOperation) {
                        $aRestArgs += $aArgs[$i]
                    } else {
                        $sOperation = $aArgs[$i]
                    }
                    break
                }
            }
        }
        if ((-Not ($sOperation)) -and ($dGlobalArgs["help"] -gt 0)) {
            # display main help screen when no operation is specified
            [wpk]::help($dGlobalArgs)
        } else {
#            try {
                Invoke-Expression "[wpk_$($sOperation)]::exec(`$dGlobalArgs, `$aRestArgs)"
#            } catch [System.Management.Automation.RuntimeException] {
#                if ($_.Exception.Message -ilike "unable to find type*") {
#                    Write-Host "ERROR: '$($sOperation)' is not a wpk command. See 'wpk --help'."
#                } else {throw $_.Exception}
#            }
        }
    }

    static execute_as_admin($sCmd) {
        if (![wpk]::oAdminProcess) {
            [wpk]::oAdminProcess = Start-Process -FilePath "powershell" -Verb runas -PassThru `
                -ArgumentList @("-NoExit", "-EncodedCommand", `
                [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes("& `"${global:wpk_script}`" admin")))
#            [wpk]::oAdminProcess = Start-Process -FilePath "powershell" -Verb runas -PassThru `
#                -WindowStyle hidden -ArgumentList @("-File","${global:wpk_script} admin")
            [wpk]::oAdminPipeOut = New-Object System.IO.Pipes.NamedPipeServerStream 'wpkadminpipeout','Out'
            [wpk]::oAdminPipeOut.WaitForConnection()
            [wpk]::oAdminPipeOutWriter = New-Object System.IO.StreamWriter([wpk]::oAdminPipeOut)
            [wpk]::oAdminPipeOutWriter.AutoFlush = $true
            
            [wpk]::oAdminPipeIn = New-Object System.Io.Pipes.NamedPipeClientStream '.','wpkadminpipein','In'
            [wpk]::oAdminPipeIn.Connect()
            [wpk]::oAdminPipeInReader = New-Object System.IO.StreamReader([wpk]::oAdminPipeIn)
        }
        [wpk]::oAdminPipeOutWriter.WriteLine($sCmd)
        while ([wpk]::oAdminPipeInReader.ReadLine() -ne 'done') {Start-Sleep -Milliseconds 10}
    }

    static cleanup() {
        if ([wpk]::oAdminProcess) {
            [wpk]::oAdminPipeOutWriter.WriteLine('exit')
            [wpk]::oAdminPipeOutWriter.Dispose()
            [wpk]::oAdminPipeOut.Dispose()
            
            while ([wpk]::oAdminPipeInReader.ReadLine() -ne 'exit') {Start-Sleep -Milliseconds 10}
            [wpk]::oAdminPipeInReader.Dispose()
            [wpk]::oAdminPipeIn.Dispose()
            
            Wait-Process [wpk]::oAdminProcess
        }
    }

    [wpk_package] static get_package_by_name($sName, $sJson) {
        try {
            . ("${global:wpk_script_directory}\repository\$($sName).ps1")
        } catch [System.Management.Automation.RuntimeException] {
            if ($_.Exception.Message -ilike "*not recognized as the name of a*") {
                Write-Host "ERROR: unsatisfiable constraints: $($sName) (missing)"
                exit 1
            } else {throw $_.Exception}
        }
        if ($sJson) {
            return ($sJson | ConvertFrom-Json) -as "wpk_package_$($sName)"
        } else {
            return New-Object -TypeName "wpk_package_$($sName)"
        }
    }
}

class wpk_add {
    static help() {
        Write-Host ${global:wpk_add_help}
    }

    static exec($dGlobalArgs, $aArgs) {
        if ($dGlobalArgs["help"] -gt 0) {[wpk_add]::help(); [wpk]::help_globalargs(); return}
        $dArgs = @{
            upgrade = 0
        }
        $aPackages = New-Object System.Collections.ArrayList
        $aInnerDepsIndices = New-Object System.Collections.ArrayList
        # read arguments
        for ($i = 0; $i -lt $aArgs.length; $i++) {
            switch -regex ($aArgs[$i].ToLower()) {
                "^(-u|--upgrade)$" {$dArgs["upgrade"]++; break}
                default {
                    $aPackages.Add([wpk]::get_package_by_name($aArgs[$i], ""))
                    break
                }
            }
        }
        # get dependencies, for now add innerdeps to deps (so their deps get processed),
        # but save the indices to easily delete them later on again
        for ($i = 0; $i -lt $aPackages.Count; $i++) {
            $aDeps = $aPackages[$i].dependencies()
            $aInnerDeps = $aPackages[$i].inner_dependencies()
            for ($j = 0; $j -lt $aDeps.length; $j++) {
                $aPackages.Add([wpk]::get_package_by_name($aDeps[$j], ""))
            }
            for ($j = 0; $j -lt $aInnerDeps.length; $j++) {
                $aInnerDepsIndices.Add($aPackages.Count)
                $aPackages.Add([wpk]::get_package_by_name($aInnerDeps[$j], ""))
            }
        }
        # remove inner dependencies from package list
        for ($i = $aInnerDepsIndices.Count - 1; $i -ge 0; $i--) {
            $aPackages.RemoveAt($aInnerDepsIndices[$i])
        }
        # download packages
        for ($i = 0; $i -lt $aPackages.Count; $i++) {
            # remove "wpk_package_" from class name is the package name
            $sName = $aPackages[$i].GetType().fullname.Substring(12)
            $sVersion = $aPackages[$i].version()
            $sFile = "${global:wpk_script_directory}\cache\$($sName)-$($sVersion).$($aPackages[$i].fileext())"
            if (($dGlobalArgs["force_refresh"]) -Or !(Test-Path $sFile)) {
                if ($dGlobalArgs["verbose"]) {
                    Write-Host "($($i + 1)/$($aPackages.Count)) downloading $($sName)-$($sVersion)"
                    Write-Host "  download url: $($aPackages[$i].download_url())"
                }
                # actually download
                $oUri = New-Object "System.Uri" $aPackages[$i].download_url()
                $oRequest = [System.Net.HttpWebRequest]::Create($oUri)
                $oRequest.set_Timeout(15000)
                try {
                    $oResponse = $oRequest.GetResponse()
                } catch {
                    # try with another user agent string
                    $oRequest.UserAgent = 'Mozilla/5.0'
                    $oResponse = $oRequest.GetResponse()
                }
                $iContentLength = $oResponse.get_ContentLength()
                $oResponseStream = $oResponse.GetResponseStream()
                New-Item -Path "${global:wpk_script_directory}\cache" -ItemType Directory -Force
                $oTargetStream = New-Object -TypeName System.IO.FileStream -ArgumentList $sFile, Create
                $bBuffer = New-Object byte[] 10KB
                $iCount = $oResponseStream.Read($bBuffer, 0, $bBuffer.length)
                $iDownloadedBytes = $iCount
                while ($iCount -gt 0) {
                    $oTargetStream.Write($bBuffer, 0, $iCount)
                    $iCount = $oResponseStream.Read($bBuffer, 0, $bBuffer.length)
                    $iDownloadedBytes += $iCount
                    if ($dGlobalArgs["verbose"]) {
                        Write-Host -NoNewLine "`r  process: $([System.Math]::Floor($iDownloadedBytes / $iContentLength * 100))%"
                    } else {
                        Write-Host -NoNewLine "`r($($i + 1)/$($aPackages.Count)) downloading $($sName)-$($sVersion) "
                        Write-Host -NoNewLine "$([System.Math]::Floor($iDownloadedBytes / $iContentLength * 100))%"
                    }
                }
                $oTargetStream.Flush()
                $oTargetStream.Close()
                $oTargetStream.Dispose()
                $oResponseStream.Dispose()
                if ($dGlobalArgs["verbose"]) {
                    Write-Host "`r  process: 100%"
                } else {
                    Write-Host "`r($($i + 1)/$($aPackages.Count)) downloading $($sName)-$($sVersion) 100%"
                }
            } else {
                Write-Host "($($i + 1)/$($aPackages.Count)) using cache for $($sName)-$($sVersion)"
            }
        }
        # checksums
        for ($i = 0; $i -lt $aPackages.Count; $i++) {
            $sName = $aPackages[$i].GetType().fullname.Substring(12)
            $sVersion = $aPackages[$i].version()
            $sFile = "${global:wpk_script_directory}\cache\$($sName)-$($sVersion).$($aPackages[$i].fileext())"
            Write-Host -NoNewLine "($($i + 1)/$($aPackages.Count)) verifying $($sName)-$($sVersion)"
            if ($dGlobalArgs["verbose"]) {Write-Host ""}
            $aChecksums = $aPackages[$i].checksums()
            for($i = 0; $i -lt $aChecksums.length; $i++) {
                $sHash = (Get-FileHash -Path $sFile -Algorithm $aChecksums[$i][0]).Hash
                if ($dGlobalArgs["verbose"]) {
                    Write-Host "  $($aChecksums[$i][0]) valid: $($aChecksums[$i][1].ToLower())"
                    Write-Host "  $($aChecksums[$i][0]) local: $($sHash.ToLower())"
                }
                if (!($sHash -ieq $aChecksums[$i][1])) {
                    if (!$dGlobalArgs["verbose"]) {Write-Host ""}
                    Write-Host "ERROR: checksums of $($sName)-$($sVersion) not matching"
                    exit 1
                } elseif ($dGlobalArgs["verbose"]) {Write-Host "  ok"}
            }
            if ($aChecksums.length) {
                if (!$dGlobalArgs["verbose"]) {Write-Host " ok"}
            } else {
                if ($dGlobalArgs["verbose"]) {Write-Host "  skipped"}
                else {Write-Host " skipped"}
            }
        }
        # install
        for ($i = 0; $i -lt $aPackages.Count; $i++) {
            $sName = $aPackages[$i].GetType().fullname.Substring(12)
            $sVersion = $aPackages[$i].version()
            Write-Host "($($i + 1)/$($aPackages.Count)) installing $($sName)-$($sVersion)"
            [wpk]::execute_as_admin("install $($sName) " + ($aPackages[$i] | Select-Object -Property * | ConvertTo-Json -Compress))
        }
    }
}

class wpk_admin {
    static exec($dGlobalArgs, $aArgs) {
        $oPipeIn = New-Object System.IO.Pipes.NamedPipeClientStream '.','wpkadminpipeout','In'
        $oPipeIn.Connect()
        $oInSR = New-Object System.IO.StreamReader($oPipeIn)
        
        $oPipeOut = New-Object System.IO.Pipes.NamedPipeServerStream 'wpkadminpipein','Out'
        $oPipeOut.WaitForConnection()
        $oOutSW = New-Object System.IO.StreamWriter($oPipeOut)
        $oOutSW.AutoFlush = $true
        
        while (($sCmd = $oInSR.ReadLine()) -ne 'exit') {
            Write-Host $sCmd
            $sCmd -match "(\S+) (.*)"
            switch ($matches[1]) {
                "install" {
                    $sCmd = $matches[2]
                    "$sCmd" -match '(\S+) (.*)'
                    $sName = $matches[1]
                    $oPackage = [wpk]::get_package_by_name("$($sName)", "$($matches[2])")
                    $sVersion = $oPackage.version()
                    $sFile = "${global:wpk_script_directory}\cache\$($sName)-$($sVersion).$($oPackage.fileext())"
                    if ([Environment]::Is64BitOperatingSystem -and !$oPackage.winver().Contains("win64")) {
                        $sInstallDir = "${env:programfiles(x86)}\$($sName)"
                    } else {
                        $sInstallDir = "${env:programfiles}\$($sName)"
                    }
                    $oPackage.pre_install()
                    $oPackage.install($sFile, $sInstallDir)
                    $oPackage.post_install()
                    
                    # create shims
                    $aPathExts = [System.Environment]::GetEnvironmentVariable('PATHEXT', [System.EnvironmentVariableTarget]::Machine) -Split ";"
                    if (!($aPathExts.Contains(".LNK"))) {
                        $aPathExts += ".LNK"
                        [System.Environment]::SetEnvironmentVariable('PATHEXT', ($aPathExts -join ";"), [System.EnvironmentVariableTarget]::Machine)
                    }
                    $aPaths = [System.Environment]::GetEnvironmentVariable('PATH', [System.EnvironmentVariableTarget]::Machine) -Split ";"
                    if (!($aPaths.Contains('C:\Shims'))) {
                        $aPaths += 'C:\Shims'
                        [System.Environment]::SetEnvironmentVariable('PATH', ($aPaths -join ";"), [System.EnvironmentVariableTarget]::Machine)
                    }
                    New-Item -Path "C:\Shims" -ItemType Directory -Force
                    $oWshShell = New-Object -comObject WScript.Shell
                    $aShims = $oPackage.shims()
                    for($i = 0; $i -lt $aShims.length; $i++) {
                        if (!(Test-Path "C:\Shims\$($aShims[$i][2]).lnk")) {
                            $oShortcut = $oWshShell.CreateShortcut("C:\Shims\$($aShims[$i][2]).lnk")
                            $oShortcut.TargetPath = "$($sInstallDir)\$($aShims[$i][0])"
                            if ($aShims[$i][1]) {$oShortcut.Arguments = $aShims[$i][1]}
                            $oShortcut.Save()
                        }
                    }
                }
            }
            Start-Sleep -Milliseconds 200
            $oOutSW.WriteLine('done')
        }
        $oInSR.Dispose()
        $oPipeIn.Dispose()
        $oOutSW.WriteLine('exit')
        $oOutSW.Dispose()
        $oPipeOut.Dispose()
    }
}

function main($aArgs) {
    if($aArgs.Count -eq 0) {
        [wpk]::help(@{})
    } else {
        [wpk]::exec($aArgs)
    }
    [wpk]::cleanup()
}

main $args
