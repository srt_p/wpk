@echo off
rem call :check_permissions

start /d "%~dp0" /b /wait powershell.exe -noLogo -ExecutionPolicy unrestricted -file "%~dp0\wpk.ps1" %*

exit /b 0

:check_permissions
    net session >nul 2>&1
    if not %errorLevel% == 0 (
        echo error: missing administrative permissions
        echo press any key to exit
        pause >nul
        exit
    )
exit /b 1
