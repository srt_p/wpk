class wpk_package_audacity : wpk_package {
    $sVersion
    $aChecksums

    [string] download_url() {
        return "http://files.snapfiles.com/directdl/Audacity-$($this.version()).zip"
    }

    [string[][]] shims() {
        return @(,@("audacity.exe", "", "audacity"))
    }

    [string[]] winver() {
        return @(,"win32")
    }

    [string] installertype() {
        return "zip"
    }

    [string] description() {
        return "Audacity is an easy-to-use, multi-track audio editor and recorder for Windows, " + `
            "Mac OS X, GNU/Linux and other operating systems, developed by a group of volunteers as open source."
    }

    [string] version() {
        if (!($this.sVersion)) {
            $sHttpContent = Invoke-WebRequest -URI "https://www.audacityteam.org/download/windows/" -UseBasicParsing
            $sHttpContent -match "<h2>Current Version: ([^<]+)</h2>"
            $this.sVersion = $Matches[1]
        }
        return $this.sVersion
    }

    [string[][]] checksums() {
        if (!$this.aChecksums) {
            $sHttpContent = Invoke-WebRequest -URI "https://wiki.audacityteam.org/wiki/Release_Notes_$($this.version())" -UseBasicParsing
            $sHttpContent -match "Audacity-[0-9.]*[.]zip\s*SHA256\s*([0-9a-f]*)"
            $sSha256Sum = $matches[1]
            $this.aChecksums = @(,@("sha256",$sSha256Sum))
        }
        return $this.aChecksums
    }
}
