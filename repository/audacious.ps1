class wpk_package_audacious : wpk_package {
    $sVersion

    [string] download_url() {
        return "https://distfiles.audacious-media-player.org/audacious-$($this.version())-win32.zip"
    }
    
    [string[][]] shims() {
        return @(,@("bin\audacious.exe", "", "audacious"))
    }

    [string[]] winver() {
        return @("win32")
    }

    [string] installertype() {
        return "zip"
    }

    [string] description() {
        return "Audacious is an open source audio player. A descendant of XMMS, Audacious plays " + `
            "your music how you want it, without stealing away your computer’s resources from other tasks."
    }

    [string] version() {
        if (!($this.sVersion)) {
            $sHttpContent = Invoke-WebRequest -URI "https://audacious-media-player.org/download" -UseBasicParsing
            $sHttpContent -match 'Current stable release: ([0-9.]*)'
            $this.sVersion = $Matches[1]
        }
        return $this.sVersion
    }
}
