class wpk_package_freecad : wpk_package {
    $sVersion
    $sDownloadUrl
    $aChecksums

    [string] download_url() {
        if (!$this.sDownloadUrl) {
            $sHttpContent = Invoke-WebRequest -URI "https://github.com/FreeCAD/FreeCAD/releases/latest" -UseBasicParsing
            if ([Environment]::Is64BitOperatingSystem) {
                $sHttpContent -match "FreeCAD-\S*-WIN-x64-portable.7z"
            } else {
                $sHttpContent -match "FreeCAD-\S*-WIN-x32-portable.7z"
            }
            $this.sDownloadUrl = "https://github.com/FreeCAD/FreeCAD/releases/download/$($this.version())/$($matches[0])"
        }
        return $this.sDownloadUrl
    }

    [string[][]] shims() {
        return @(,@("conda-$($this.version())\bin\FreeCAD.exe", "", "freecad"))
    }

    [string] installertype() {
        return "7z"
    }

    [string] description() {
        return "FreeCAD is a 3D CAD/CAE parametric modeling application. It is primarily " + `
            "made for mechanical design, but also serves all other uses where you need to model " + `
            "3D objects with precision and control over modeling history."
    }

    [string] version() {
        if (!($this.sVersion)) {
            $sHttpContent = Invoke-WebRequest -URI "https://github.com/FreeCAD/FreeCAD/releases/latest" -UseBasicParsing
            $sHttpContent -match "FreeCAD ([0-9.]+)"
            $this.sVersion = $Matches[1]
        }
        return $this.sVersion
    }

    [string[][]] checksums() {
        if (!$this.aChecksums) {
            $sHttpContent = Invoke-WebRequest -URI "$($this.download_url())-SHA256.txt" -UseBasicParsing
            $sHttpContent -match '.7z:\s*(([0-9a-f]{2}\s+)+)'
            $this.aChecksums = @(,@("sha256",($matches[1] -replace "\s","")))
        }
        return $this.aChecksums
    }
}
