class wpk_package_7zip : wpk_package {
    $sVersion
    $aChecksums

    [string] download_url() {
        $sShortVersion = $this.version() -replace "\.", ""
        if ([Environment]::Is64BitOperatingSystem) {
            return "https://www.7-zip.org/a/7z$($sShortVersion)-x64.msi"
        } else {
            return "https://www.7-zip.org/a/7z$($sShortVersion).msi"
        }
    }

    [string[][]] shims() {
        return @(@("7z.exe", "", "7z"),@("7zFM.exe", "", "7zfm"))
    }

    [string] installertype() {
        return "msi"
    }

    [string] description() {
        return "7-Zip is a free and open-source file archiver, a utility used to place groups of files within compressed containers known as " + `
        "`"archives`". It is developed by Igor Pavlov and was first released in 1999."
    }

    [string] version() {
        if (!($this.sVersion)) {
            $sHttpContent = Invoke-WebRequest -URI "https://www.7-zip.org/" -UseBasicParsing
            $sHttpContent -match "Download 7-Zip ([0-9]+[.][0-9]+) [(]"
            $this.sVersion = $Matches[1]
        }
        return $this.sVersion
    }
}
