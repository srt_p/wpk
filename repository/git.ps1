class wpk_package_git : wpk_package {
    $sDownloadUrl
    $sVersion
    $aChecksums

    [string] download_url() {
        if (!($this.sDownloadUrl)) {
            $sHttpContent = Invoke-WebRequest -URI "https://git-scm.com/download/win" -UseBasicParsing
            if ([Environment]::Is64BitOperatingSystem) {
                $sHttpContent -match '<a href="([^"]*)">64-bit Git for Windows Setup</a>'
            } else {
                $sHttpContent -match '<a href="([^"]*)">32-bit Git for Windows Setup</a>'
            }
            $this.sDownloadUrl = $matches[1]
        }
        return $this.sDownloadUrl
    }

    [string[][]] shims() {
        return @(@("cmd\git-gui.exe", "", "git-gui"),@("cmd\git.exe", "", "git"))
    }

    [string] installertype() {
        return "innosetup"
    }

    [string] description() {
        return "Git is a distributed version-control system for tracking changes in source code " + `
            "during software development.[8] It is designed for coordinating work among programmers, " + `
            "but it can be used to track changes in any set of files."
    }

    [string] version() {
        if (!($this.sVersion)) {
            $sHttpContent = Invoke-WebRequest -URI "https://git-scm.com/download/win" -UseBasicParsing
            $sHttpContent -match 'You are downloading the latest [(]<strong>([0-9.]*)</strong>[)]'
            $this.sVersion = $matches[1]
        }
        return $this.sVersion
    }
}
