class wpk_package_vlc : wpk_package {
    $sVersion
    $sDownloadUrl
    $aChecksums

    [string] download_url() {
        if (!$this.sDownloadUrl) {
            if ([Environment]::Is64BitOperatingSystem) {
                $sHttpContent = Invoke-WebRequest -URI "https://get.videolan.org/vlc/$($this.version())/win64/vlc-$($this.version())-win64.zip" -UseBasicParsing
            } else {
                $sHttpContent = Invoke-WebRequest -URI "https://get.videolan.org/vlc/$($this.version())/win32/vlc-$($this.version())-win32.zip" -UseBasicParsing
            }
            $sHttpContent -match "If not, <a.*href=`"([^`"]+)`".*>click here</a>"
            $this.sDownloadUrl = $matches[1]
        }
        return $this.sDownloadUrl
    }

    [string[][]] shims() {
        return @(,@("vlc-$($this.version())\vlc.exe", "", "vlc"))
    }

    [string] installertype() {
        return "zip"
    }

    [string] description() {
        return "VLC media player is a free and open-source, portable, cross-platform " + `
            "media player and streaming media server developed by the VideoLAN project."
    }

    [string] version() {
        if (!($this.sVersion)) {
            $sHttpContent = Invoke-WebRequest -URI "https://www.videolan.org/vlc/download-sources.html" -UseBasicParsing
            $sHttpContent -match "<p> Latest VLC source code tarball [(]([^)]+)[)]:</p>"
            $this.sVersion = $Matches[1]
        }
        return $this.sVersion
    }

    [string[][]] checksums() {
        if (!$this.aChecksums) {
            if ([Environment]::Is64BitOperatingSystem) {
                $sWinver = "win64"
            } else {
                $sWinver = "win32"
            }
            $sHttpContent = Invoke-WebRequest -URI "https://download.videolan.org/pub/videolan/vlc/$($this.version())/$($sWinver)/vlc-$($this.version())-$($sWinver).zip.md5" -UseBasicParsing
            $sHttpContent -match "(\S)*"
            $sMd5Sum = $matches[0]
            $sHttpContent = Invoke-WebRequest -URI "https://download.videolan.org/pub/videolan/vlc/$($this.version())/$($sWinver)/vlc-$($this.version())-$($sWinver).zip.sha1" -UseBasicParsing
            $sHttpContent -match "(\S)*"
            $sSha1Sum = $matches[0]
            $sHttpContent = Invoke-WebRequest -URI "https://download.videolan.org/pub/videolan/vlc/$($this.version())/$($sWinver)/vlc-$($this.version())-$($sWinver).zip.sha256" -UseBasicParsing
            $sHttpContent -match "(\S)*"
            $sSha256Sum = $matches[0]
            $this.aChecksums = @(@("md5",$sMd5Sum),@("sha1",$sSha1Sum),@("sha256",$sSha256Sum))
        }
        return $this.aChecksums
    }
}
