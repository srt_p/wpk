class wpk_package_blender : wpk_package {
    $sVersion
    $aChecksums

    [string] download_url() {
        if ([Environment]::Is64BitOperatingSystem) {
            return "http://ftp.halifax.rwth-aachen.de/blender/release/Blender$($this.shortversion1())/blender-$($this.version())-windows64.zip"
        } else {
            return "http://ftp.halifax.rwth-aachen.de/blender/release/Blender$($this.shortversion1())/blender-$($this.version())-windows32.zip"
        }
    }

    [string[][]] shims() {
        if ([Environment]::Is64BitOperatingSystem) {
            return @(,@("blender-$($this.version())-windows64\blender.exe", "", "blender"))
        } else {
            return @(,@("blender-$($this.version())-windows32\blender.exe", "", "blender"))
        }
    }

    [string] installertype() {
        return "zip"
    }

    [string] description() {
        return "Blender is the free and open source 3D creation suite. It supports the entirety " + `
            "of the 3D pipeline\u2014modeling, rigging, animation, simulation, rendering, compositing " + `
            "and motion tracking, even video editing and game creation."
    }

    [string] version() {
        if (!($this.sVersion)) {
            $sHttpContent = Invoke-WebRequest -URI "https://www.blender.org/download/" -UseBasicParsing
            $sHttpContent -match 'blender-([0-9a-z.]*).tar.xz'
            $this.sVersion = $Matches[1]
        }
        return $this.sVersion
    }

    [string] shortversion1() {
        # shortversion with only numeric and dots
        $this.version() -match '([0-9.]*)'
        return $matches[1]
    }

    [string] shortversion2() {
        # shortversion without dots
        return $this.version() -replace "[.]",""
    }

    [string[][]] checksums() {
        if (!$this.aChecksums) {
            if ([Environment]::Is64BitOperatingSystem) {
                $sWinver = "64"
            } else {
                $sWinver = "32"
            }
            $sHttpContent = Invoke-WebRequest -URI "http://ftp.halifax.rwth-aachen.de/blender/release/Blender$($this.shortversion1())/release$($this.shortversion2()).md5" -UseBasicParsing
            $sHttpContent -match "([0-9a-f]+)\s+blender-$($this.version())-windows$($sWinVer).zip"
            $sMd5Sum = $matches[1]
            $sHttpContent = Invoke-WebRequest -URI "http://ftp.halifax.rwth-aachen.de/blender/release/Blender$($this.shortversion1())/release$($this.shortversion2()).sha256" -UseBasicParsing
            $sHttpContent -match "([0-9a-f]+)\s+blender-$($this.version())-windows$($sWinVer).zip"
            $sSha256Sum = $matches[1]
            $this.aChecksums = @(@("md5",$sMd5Sum),@("sha256",$sSha256Sum))
        }
        return $this.aChecksums
    }
}
