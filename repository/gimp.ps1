class wpk_package_gimp : wpk_package {
    $sDownloadUrl
    $sVersion
    $aChecksums

    [string] download_url() {
        if (!$this.sDownloadUrl) {
            # installer number
            $sHttpContent = Invoke-WebRequest -URI "https://download.gimp.org/mirror/pub/gimp/v$($this.shortversion())/windows/" -UseBasicParsing
            $sPattern = 'gimp-' + ($this.version() -replace '\.', '\.') + '-setup(-[0-9]+)*\.exe'
            $aMatches = ([regex]$sPattern).Matches($sHttpContent)
            $iSetup = 0
            foreach($sMatch in $aMatches) {
                if ($sMatch.Value -match 'setup-([0-9]+)\.exe') {$iSetup = [System.Math]::Max($iSetup, $Matches[1])}
            }
            $sUrl = "https://download.gimp.org/mirror/pub/gimp/v$($this.shortversion())/windows/gimp-$($this.version())-setup"
            if($iSetup) {$sUrl += "-$iSetup"}
            $this.sDownloadUrl = "$($sUrl).exe"
        }
        return $this.sDownloadUrl
    }

    [string[][]] shims() {
        return @(,@("bin\gimp-$($this.shortversion()).exe", "", "gimp"))
    }

    [string] installertype() {
        return "innosetup"
    }

    [string] description() {
        return "GIMP (GNU Image Manipulation Program) is a free and open-source " + `
            "raster graphics editor used for image retouching and editing, free-form " + `
            "drawing, converting between different image formats, and more specialized tasks."
    }

    [string] version() {
        if (!($this.sVersion)) {
            $sHttpContent = Invoke-WebRequest -URI "https://www.gimp.org/downloads/" -UseBasicParsing
            $sHttpContent -match 'The current stable release of GIMP is <b>(?<Version>[0-9.]*)</b>'
            $this.sVersion = $Matches.Version
        }
        return $this.sVersion
    }

    [string] shortversion() {
        # short version string
        $this.version() -match '([0-9.]*)[.][0-9]+'
        return $Matches[1]
    }

    [string[][]] checksums() {
        if (!$this.aChecksums) {
            $sRemoteFilename = ($this.download_url() -split '/')[-1]
            $sHttpContent = Invoke-WebRequest -URI "https://download.gimp.org/mirror/pub/gimp/v$($this.shortversion())/windows/SHA256SUMS" -UseBasicParsing
            $sHttpContent -match "(\S)+\s+$($sRemoteFilename)"
            $sSha256Sum = ($matches[0] -split " ")[0]
            $sHttpContent = Invoke-WebRequest -URI "https://download.gimp.org/mirror/pub/gimp/v$($this.shortversion())/windows/SHA512SUMS" -UseBasicParsing
            $sHttpContent -match "(\S)+\s+$($sRemoteFilename)"
            $sSha512Sum = ($matches[0] -split " ")[0]
            $this.aChecksums = @(@("sha256",$sSha256Sum),@("sha512",$sSha512Sum))
        }
        return $this.aChecksums
    }
}
