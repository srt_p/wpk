class wpk_package_ffmpeg : wpk_package {
    $sVersion

    [string] download_url() {
        if ([Environment]::Is64BitOperatingSystem) {
            return "https://ffmpeg.zeranoe.com/builds/win64/static/ffmpeg-$($this.version())-win64-static.zip"
        } else {
            return "https://ffmpeg.zeranoe.com/builds/win32/static/ffmpeg-$($this.version())-win32-static.zip"
        }
    }

    [string[][]] shims() {
        if ([Environment]::Is64BitOperatingSystem) {
            $sWinVer = "win64"
        } else {
            $sWinVer = "win32"
        }
        return @(@("ffmpeg-$($this.version())-$($sWinVer)-static\bin\ffmpeg.exe", "", "ffmpeg"),@("ffmpeg-$($this.version())-$($sWinVer)-static\bin\ffplay.exe", "", "ffplay"),@("ffmpeg-$($this.version())-$($sWinVer)-static\bin\ffprobe.exe", "", "ffprobe"))
    }

    [string] installertype() {
        return "zip"
    }

    [string] description() {
        return "FFmpeg is the leading multimedia framework, able to decode, encode, transcode," + `
            "mux, demux, stream, filter and play pretty much anything that humans and machines have created."
    }

    [string] version() {
        if (!($this.sVersion)) {
            $sHttpContent = Invoke-WebRequest -URI "https://ffmpeg.org/download.html" -UseBasicParsing
            $sHttpContent -match "<small>ffmpeg-([0-9.]*)[.]tar[.]bz2</small>"
            $this.sVersion = $Matches[1]
        }
        return $this.sVersion
    }
}
