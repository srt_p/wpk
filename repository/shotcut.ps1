class wpk_package_shotcut : wpk_package {
    $sDownloadUrl
    $sVersion
    $aChecksums

    [string] download_url() {
        if ([Environment]::Is64BitOperatingSystem) {
            return "https://github.com/mltframework/shotcut/releases/download/v$($this.version())/shotcut-win64-$($this.shortversion()).exe"
        } else {
            return "https://github.com/mltframework/shotcut/releases/download/v$($this.version())/shotcut-win32-$($this.shortversion()).exe"
        }
    }

    [string[][]] shims() {
        return @(,@("shotcut.exe", "", "shotcut"))
    }

    [string] installertype() {
        return "nsis"
    }

    [string] description() {
        return "Shotcut is a free, open source, cross-platform video editor."
    }

    [string] version() {
        if (!($this.sVersion)) {
            $sHttpContent = Invoke-WebRequest -URI "https://www.shotcut.org/download/" -UseBasicParsing
            $sHttpContent -match 'Current Version: ([0-9.]*)'
            $this.sVersion = $matches[1]
        }
        return $this.sVersion
    }

    [string] shortversion() {
        return $this.version() -replace "[.]",""
    }

#    [string[][]] checksums() {
#        if (!$this.aChecksums) {
#            if ([Environment]::Is64BitOperatingSystem) {
#                $sWinver = "64"
#            } else {
#                $sWinver = "32"
#            }
#            $sHttpContent = Invoke-WebRequest -URI "http://ftp.halifax.rwth-aachen.de/blender/release/Blender$($this.shortversion1())/release$($this.shortversion2()).md5" -UseBasicParsing
#            $sHttpContent -match "([0-9a-f]+)\s+blender-$($this.version())-windows$($sWinVer).zip"
#            $sMd5Sum = $matches[1]
#            $sHttpContent = Invoke-WebRequest -URI "http://ftp.halifax.rwth-aachen.de/blender/release/Blender$($this.shortversion1())/release$($this.shortversion2()).sha256" -UseBasicParsing
#            $sHttpContent -match "([0-9a-f]+)\s+blender-$($this.version())-windows$($sWinVer).zip"
#            $sSha256Sum = $matches[1]
#            $this.aChecksums = @(@("md5",$sMd5Sum),@("sha256",$sSha256Sum))
#        }
#        return $this.aChecksums
#    }
}
